# pgCloudHacker

A tiny browser extension to enable the "Test in Gitpod" button for all .patch links on postgresql.org – 1-click testing of Postgres patches using Gitpod workspaces (GitLab login required).

<img src="/uploads/f6d30c84fe7073bafcff258396a5feff/test-in-gitpod.png" width="450" border="1" />

## Installation
Install it to Chrome (should work in Chromium-based browsers such as Opera and Firefox but not yet tested):

1. Download .zip: https://gitlab.com/postgres-ai/pgcloudhacker/-/archive/main/pgcloudhacker-main.zip
2. Unzip
3. Load it via "Manage extensions" > "Load unpacked":

    <img src="/uploads/c1937a6dc0725a3ae4d06ac0ac852315/chrome-load-unpacked.png" width="300" border="1" />
