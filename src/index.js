let gitlabUrl = localStorage.getItem("gitlabUrl");
const anchorLinks = document.getElementsByTagName("a");
const pageUrl = encodeURIComponent(window.location.href);

const appendButton = () => {
  for (let i = 0; i < anchorLinks.length; i++) {
    if (anchorLinks[i].href.endsWith(".patch")) {
      const encodedLink = encodeURIComponent(anchorLinks[i].href);
      const gitPodLink = `https://gitpod.io/#patchUrl=${encodedLink},pageUrl=${pageUrl}/${gitlabUrl}`;

      anchorLinks[i].addEventListener("mouseenter", () => {
        const div = document.createElement("div");
        let parent = anchorLinks[i].parentElement;
        div.className = "gitpod-div";
        div.innerHTML = "Open in Gitpod";

        anchorLinks[i].appendChild(div);
        anchorLinks[i].style.color = "#428bca";
        anchorLinks[i].style.position = "relative";

        while (parent.tagName !== "TABLE") {
          parent = parent.parentElement;
          parent.style.overflow = "visible";
        }

        div.addEventListener("click", (e) => {
          e.preventDefault();
          window.open(gitPodLink, "_blank");
        });

        document.addEventListener("mouseover", (event) => {
          if (
            event.clientY >
              anchorLinks[i].getBoundingClientRect().bottom + 30 ||
            event.clientX < anchorLinks[i].getBoundingClientRect().left ||
            event.clientX > anchorLinks[i].getBoundingClientRect().right ||
            event.clientY < anchorLinks[i].getBoundingClientRect().top
          ) {
            div.remove();
          }
        });
      });
    }
  }
};

if (gitlabUrl) appendButton();

chrome.runtime.onMessage.addListener((message) => {
  if (message.gitlabUrl !== gitlabUrl) {
    gitlabUrl = message.gitlabUrl;
    localStorage.setItem("gitlabUrl", message.gitlabUrl);
    appendButton();
  }
});
